/*
## Задание

Написать реализацию функции, которая позволит создавать объекты типа Hamburger, используя возможности стандарта ES5.


*/


const sizeSmall = {
    name: 'small',
    cost: 50,
    calories: 20
}

const sizeLarge = {
    name: 'large',
    cost: 100,
    calories: 40
}

const stuffingCheese = {
    name: 'cheese',
    cost: 10,
    calories: 20
}

const stuffingSalad = {
    name: 'salad',
    cost: 20,
    calories: 5
}

const stuffingPotato = {
    name: 'potato',
    cost: 15,
    calories: 10
}

const toppingMayo = {
    name: 'mayo',
    cost: 20,
    calories: 5
}

const toppingSpice = {
    name: 'spice',
    cost: 15,
    calories: 0
}



function Hamburger(size, stuffing) { 
    this.size = size;
    this.stuffing =[];
    for(let i = 1; i < arguments.length; i++){

        this.stuffing.push(arguments[i]);
    }
    this.arrTopping = [];
    try {
        if(size != sizeSmall && size != sizeLarge){
            throw new HamburgerException('размер введен не корректно');
        } 
        let correctStuffing = true;
        let tempArr = [];
        this.stuffing.forEach((el) => {
            if(tempArr.indexOf(el) != -1){
                correctStuffing = false;
            }
            tempArr.push(el);
            if(el != stuffingCheese && el != stuffingSalad && el != stuffingPotato){
                correctStuffing = false; 
            }
        })
        if(!correctStuffing || this.stuffing.length === 0)  {
            throw HamburgerException('добавка введена некорректно');

        } 
    } catch(e) {
        console.log(e.message);
    }


    
    this.addTopping = function(addedTopping){
        let x = argsToArray(arguments);
        try{
            if(checkСorrectnessTopping(x) == false){
                throw new HamburgerException('добавка указанна неправильно');
            }
            for(let i = 0; i < arguments.length; i++){
                if(this.arrTopping.indexOf(arguments[i]) != -1){
                    throw new HamburgerException('нельзя добавить уже существующую добавку');
                }
            }
            if(x.length === 0){
                throw new HamburgerException('ошибка добавки, обязателен аргумент');
            }
            for(let i = 0; i < arguments.length; i++){
                this.arrTopping.push(arguments[i]);
            }
        }
        catch(e){
            console.log(e.message);
        }
        
    }


    this.removeTopping = function(args){
        let x = argsToArray(arguments);
        for(let i = 0; i < arguments.length; i++){   
            try{
                if(this.arrTopping.indexOf(x[i]) != -1){
                    this.arrTopping.splice(this.arrTopping.indexOf(x[i]), 1);
                } else {
                    throw new HamburgerException('удалить можно только добавленную добавку');
                }
            }
            catch(e){
                console.log(e);
            }         
        }
    }


    this.addStuffing = function(args){
        let x = argsToArray(arguments);
        try{
            if(checkСorrectnessStuffing(x) == false){ 
                throw new HamburgerException('начинка указанна неправильно');
            }
            for(let i = 0; i < arguments.length; i++){
                if(this.stuffing.indexOf(arguments[i]) != -1){
                    throw new HamburgerException('нельзя добавить уже существующую начинку')
                }
            }
            if(x.length === 0){
                throw new HamburgerException('ошибка добавления начинки, обязателен аргумент');
            }
            for(let i = 0; i < arguments.length; i++){
                this.stuffing.push(arguments[i]);
            }
        }
        catch(e){
            console.log(e);
        }
        
    }


    this.removeStuffing = function() {
        let x = argsToArray(arguments);
        for(let i = 0; i < arguments.length; i++){   
            try{
                if(this.stuffing.indexOf(x[i]) != -1){
                    this.stuffing.splice(this.stuffing.indexOf(x[i]), 1);
                } else {
                    throw new HamburgerException('удалить можно только добавленную начинку');
                }
            }
            catch(e){
                console.log(e);
            }         
        }
    }


    this.getToppings = function(){
        if (this.arrTopping.length == 0) {
            return 'нет добавки';
        } else{
            return this.arrTopping;
        } 
    }

    this.getStuffing = function() {
        if (this.stuffing.length == 0) {
            return 'нет начинки';
        } else{
            return this.stuffing;
        } 
    }


    this.calculateCalories = function() {
        let counterCalories = this.size.calories;
        this.stuffing.forEach((el) => counterCalories += el.calories);
        this.arrTopping.forEach((el) => counterCalories += el.calories);
        
        return counterCalories;
    }
    

    this.calculatePrice = function(){
        let counterPrice = this.size.cost;
        this.stuffing.forEach((el) => counterPrice += el.cost);
        this.arrTopping.forEach((el) => counterPrice += el.cost);
        return `Цена: ${counterPrice}тгр.`;
    }

    
    this.getSize = function() {
        return this.size.name;
    }

    function HamburgerException(message) {
        this.name = 'HamburgerException';
        this.message = message;
    }


    function checkСorrectnessTopping(args){
        let tempArgs = args;
        let correctTopping = true;
        let tempArr = [];
        tempArgs.forEach((el) => {
            if(tempArr.indexOf(el) == -1){
                tempArr.push(el);  
            } else {
                correctTopping = false;
            }           
            if((el != toppingMayo && el != toppingSpice) || arguments.length === 0){
                correctTopping = false; 
            }
        })
        return correctTopping;
    }


    function checkСorrectnessStuffing(args) {
        let tempArgs = args;
        let correctStuffing = true;
        let tempArr = [];
        tempArgs.forEach((el) => {
            if(tempArr.indexOf(el) == -1){
                tempArr.push(el);  
            } else {
                correctStuffing = false;
            } 
            if((el != stuffingCheese && el != stuffingSalad && el != stuffingPotato) || arguments.length === 0){
                correctStuffing = false; 
            }
        })
        return correctStuffing;  
    }


    function argsToArray(a) {
        let arr = [];
        for(let i = 0; i < a.length; i++) {
            arr.push(a[i]);
        }
        return arr;
    }
 } 


